//
//  UIViewControllerExtension.h
//  RDPSDK
//
//  Created by Ricky Putra Harlim on 5/4/17.
//  Copyright © 2017 d'Amigos. All rights reserved.
//


#import <UIKit/UIKit.h>
/**
 Class to represent as UIViewController Extension 
 */
@interface UIViewControllerExtension : UIViewController

/**
 	Method that called to show loading pop up on screen
 	@param delegate layout or viewcontroller that will present the loading pop up
 */
+ (void)showLoadingPopUp:(UIViewController*) delegate;
/**
 	Method that called to dismiss loading pop up on screen
 	@param delegate layout or viewcontroller that have pop up or view controller to dismiss
 */
+ (void)dismissPopUp:(UIViewController*)delegate;
/**
	Method that called to dismiss loading pop up on screen using dispatch_async and also completion after dismiss
 	@param delegate layout or viewcontroller that have pop up or view controller to dismiss
 	@param completion function or step that will run after first dismiss pop is success or complete
*/
+ (void)dismissPopUpUsingDispatch:(UIViewController *)delegete  completion: (void (^ __nullable)(void))completion;
/**
	Method that called to show one viewcontroller using dispatch_async
 	@param delegate layout or viewcontroller that will present the loading pop up
	@param viewController viewcontroller that we want to show on the existing or delegate param that provide
*/
+ (void)showViewControllerPresentWithDispatch:(UIViewController*) delegate withviewController:(UIViewController *)viewController;


@end

