//
//  RDPConnectAPIWebViewController.h
//  RDP SDK
//
//  Created by Ricky Putra Harlim on 5/2/17.
//  Copyright © 2017 d'Amigos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDPConnectAPI.h"
#import "SignatureUtil.h"
#import "UIViewControllerExtension.h"
#import "ParameterExtractor.h"
#import "OriginViewType.h"

/**
 	Class to represent a RDPConnectAPIWebViewController
 */
@interface RDPConnectAPIWebViewController : UIViewController<UIWebViewDelegate, ConnectAPIInternalDelegate>
/**
	represent RDPConnectAPI
*/
@property(nonatomic, strong) RDPConnectAPI* connectAPIWrapper;
/**
	a web view to load url payment page 
*/
@property (strong, nonatomic) UIWebView *webView;
/**
	UIBarButtonItem that called and use to trigger click 
*/
@property (strong, nonatomic) UIBarButtonItem *btnNavDone;
/**
	the delegate variabel for RDPConnectAPIDelegate
*/
@property (strong, nonatomic) id<RDPConnectAPIDelegate> delegate;
/**
  	transactiontype enum that of OrifinOfView that segue to this class or viewcontroller
*/
@property enum OriginViewType origin_type;
/**
	the key that given by RDP
*/
@property NSString* secret_key;
/**
	Unique for each transaction – max 20 chars, order number is alphanumeric characters. e.g. DT13210
*/
@property NSString* order_number;
/**
	RDP Connect merchant ID – 10 chars, Obtained from Red Dot Payment
*/
@property NSString* merchant_id;
/**
	RDP Connect merchant key – 40 chars, Obtained from Red Dot Payment
*/
@property NSString* key;
/**
	Transaction amount, the numbers only
*/

@property float amount;
/**
	In 3 digits ISO-4217 Alphabetical Currency Code format, according to your merchant setup with RDP
*/
@property NSString* currency_code;
/**
	Customer or Buyer email address
*/
@property NSString* email;
/**
  	transactiontype enum that represent : Sale and Authorization.
*/
@property enum RDPConnectAPITransactionType transaction_type;
/**
	Merchant return URL, where customers will be redirected to after payment is completed.
*/
@property NSString* return_url;
/**
	Card number to be passed thru to the bank.
*/
@property NSString* card_number;
/**
	Expiry date (in MMYY format) to be passed thru to the bank.
*/
@property NSString* expiry_date;
/**
	CVV2 to be passed thru to the bank. 
*/
@property NSString* cvv2;

@end
