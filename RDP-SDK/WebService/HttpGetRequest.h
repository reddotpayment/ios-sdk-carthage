//
//  HttpGetRequest.h
//  RDP SDK
//
//  Created by Ferico Samuel on 5/1/17.
//  Copyright © 2017 d'Amigos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpRequest.h"
/**
 	Class to represent a HttpGetRequest class
 */
@interface HttpGetRequest : HttpRequest
/**
    method that called to use HTTP Request with action get
    @param isUsingHeader boolean to identify are this request using header for auth or sommething else
    @param url the url that we want to request to
    @param responseObject the response of object that will parse the return object
    @param delegate the object that receive the delegate / callback from HTTPRequest
    @returns id object of this class that has been initialized
*/
- (id) initUsingHeader:(bool)isUsingHeader withURL:(NSURL*) url andResponseObject:(WsResponse*) responseObject andDelegate:(id) delegate;

@end
