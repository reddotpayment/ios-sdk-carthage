//
//  HttpPostRequest.h
//  RDP SDK
//
//  Created by Ferico Samuel on 5/1/17.
//  Copyright © 2017 d'Amigos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpRequest.h"
/**
 	Class to represent a HttpPostRequest class
 */
@interface HttpPostRequest : HttpRequest
/**
    method that called to use HTTP Request with action post with data to send in string from
    @param isUsingHeader boolean to identify are this request using header for auth or sommething else
    @param url the url that we want to request to
    @param data the data that will send to server in string form
    @param responseObject the response of object that will parse the return object
    @param delegate the object that receive the delegate / callback from HTTPRequest
    @returns id object of this class that has been initialized
*/
- (id) initUsingHeader:(bool) isUsingHeader andURL:(NSURL*) url andDataString:(NSString*) data andResponseObject:(WsResponse*) responseObject andDelegate:(id) delegate;
/**
    method that called to use HTTP Request with action post with data to send in nsdata form
    @param isUsingHeader boolean to identify are this request using header for auth or sommething else
    @param url the url that we want to request to
    @param data the data that will send to server in NsData form
    @param responseObject the response of object that will parse the return object
    @param delegate the object that receive the delegate / callback from HTTPRequest
    @returns id object of this class that has been initialized
*/
- (id) initUsingHeader:(bool) isUsingHeader andURL:(NSURL*) url andData:(NSData*) data andResponseObject:(WsResponse*) responseObject andDelegate:(id) delegate;
/**
    method that called to use HTTP Request with action post without data to send
    @param isUsingHeader boolean to identify are this request using header for auth or sommething else
    @param url the url that we want to request to
    @param responseObject the response of object that will parse the return object
    @param delegate the object that receive the delegate / callback from HTTPRequest
    @returns id object of this class that has been initialized
*/
- (id) initUsingHeader:(bool) isUsingHeader andURL:(NSURL*) url andResponseObject:(WsResponse*) responseObject andDelegate:(id) delegate;

@end
