//
//  HttpRequest.h
//  RDP SDK
//
//  Created by Ferico Samuel on 5/1/17.
//  Copyright © 2017 d'Amigos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WsResponse.h"
/**
 Set of methods to be implemented to act as a WsDelegate
 */
@protocol WsDelegate <NSObject>
/**
    method that called when request is success
    @param responseObject the object in json that we parse so we can get the value that return
*/
- (void) onSuccessWith:(WsResponse*)responseObject;
/**
    method that called when request is error
    @param error the message that given by server that cause request is failed or error
    @param request identifier that do the request
*/
- (void) onErrorWith: (NSString*) error andErrorCode: (NSString*) errorCode andRequest:(id) request;
/**
    method that called when request is failed because is no network available
    @param request identifier that do the request
*/
- (void) onNoNetwork: (id) request;

@end
/**
 	Class to represent a HttpRequest
 */
@interface HttpRequest : NSObject


/**
	the delegate variabel for WsDelegate
*/
@property(nonatomic, strong) id<WsDelegate> delegate;
/**
	boolean variabel to decide is request using header or not
*/
@property(nonatomic) bool isUsingHeader;
/**
	request that will be use to call API
*/
@property(nonatomic, strong) NSMutableURLRequest* request;
/**
	return value from server in WsResponse class
*/
@property(nonatomic, strong) WsResponse* responseObject;
/**
 	Method that called to make a request that has been initialized
 	@param isUsingHeader boolean to send a header or not
 */
- (void) makeRequest: (bool)isUsingHeader;
/**
 	Method that called to initialize the delegate of this class 
 	@param delegate the param to set the context for implement WsDelegate
 	@returns id the intial of HttpRequest
 */
- (id) initWithDelegate: (id<WsDelegate>) delegate andURL:(NSURL*) url andResponseObject:(WsResponse*) responseObject;
/**
 	Method that called to retry request
 */
- (void) retry;

@end
