//
//  WsResponse.h
//  RDP SDK
//
//  Created by Ferico Samuel on 5/1/17.
//  Copyright © 2017 d'Amigos. All rights reserved.
//

#import <Foundation/Foundation.h>
#define SUCCESS_RESPONSE_CODE "0"
/**
 	Class to represent a WsResponse
 */
@interface WsResponse : NSObject

/**
	the response message that default be return on json with key response_msg
*/
@property(nonatomic, strong) NSString* response_msg;
/**
	the response message that default be return on json with key response_code
*/
@property(nonatomic, strong) NSString* response_code;
/**
	the response JsonObject that has been parse using JSOnserialization
*/
@property(nonatomic, strong) id jsonObject;
/**
 	Method that called to parse data to json object
 	@param data the data that provide or return from API to parse
 */
- (void) parse :(NSData*)data;
/**
 	Method that called to check is the response is success or not
 	@returns the success value YES if success and NO if error.
 */
- (BOOL) isSuccess;

@end
