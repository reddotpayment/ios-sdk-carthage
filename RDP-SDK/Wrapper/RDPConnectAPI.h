//
//  RDPConnectAPI.h
//  RDPSDK
//
//  Created by Ricky Putra Harlim on 5/8/17.
//  Copyright © 2017 d'Amigos. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "RDPConnectAPITransactionType.h"
#import "QueryStringBuilder.h"
#import "HttpPostRequest.h"
#import "Util.h"
#import "StringResponse.h"
#import "ConnectAPIInternalDelegate.h"
#define RDP_CAPI_DEV_URL @"http://test.reddotpayment.com/connect-api/cgi-bin-live"
#define RDP_CAPI_PRODUCTION_URL @"https://connect.reddotpayment.com/merchant/cgi-bin-live"

/**
 Set of methods to be implemented to act as a Connect API (CAPI)
 */
@protocol RDPConnectAPIDelegate <NSObject>
/**
 Method that called when request transaction is failed
 @param errorCode HTTP Response Code
 @param description Description Of Error
 */
- (void) onRequestFailedWithErrorCode:(int) errorCode andDescription:(NSString*) description;
/**
 Method that called when request rejected 
 @param params the dictionary or response that return from server
 @param errorCode Error Code Value from HTTP Response Code
 */
- (void) onRequestRejectedWithParams:(NSDictionary*) params andErrorCode:(int) errorCode;
/**
 Method that called when request transaction is Finished or Success
 @param params the dictionary or response that return from server
 */
- (void) onRequestFinishedWithParams:(NSDictionary*) params;

@optional
/**
 Method that called just before the payment request
 */
- (void) paymentInterfaceWillRequest;
/**
 Method that called just after the payment Request
 */
- (void) paymentInterfaceDidRequest;
/**
 Method that called just before the return url load
 */
- (void) returnUrlWillLoad;
/**
 Method that called just after the return url load
 */
- (void) returnUrlDidLoad;
/**
 Method that called just before the signature validate
 */
- (void) transactionSignatureWillValidate;
/**
 Method that called just after the signature has been validated
 @param params the dictionary of result signature validation
 */
- (void) transactionSignatureDidValidateWithParam: (NSDictionary*) params;
/**
 Method that call when consent screen is will be appear or not.
 This method default falue is NO
 @returns returns YES if you want to dismiss consent screen, otherwise returns NO.
 */
- (BOOL) shouldDismissConsentScreen;
/**
 Method that called just after the consent screen dissapear
 */
- (void) consentScreenDidDissapear;
@end

/**
 	Class to represent a RDP Connect API (CAPI)
 */
@interface RDPConnectAPI : NSObject<WsDelegate>

/**
	the key that given by RDP
*/
@property(nonatomic, strong) NSString* secret_key;
/**
	Unique for each transaction – max 20 chars, order number is alphanumeric characters. e.g. DT13210
*/
@property(nonatomic, strong) NSString* order_number;
/**
	RDP Connect merchant ID – 10 chars, Obtained from Red Dot Payment
*/
@property(nonatomic, strong) NSString* merchant_id;
/**
	RDP Connect merchant key – 40 chars, Obtained from Red Dot Payment
*/
@property(nonatomic, strong) NSString* key;
/**
	Transaction amount, the numbers only
*/
@property(nonatomic) float amount;
/**
	In 3 digits ISO-4217 Alphabetical Currency Code format, according to your merchant setup with RDP
*/
@property(nonatomic, strong) NSString* currency_code;
/**
	Customer or Buyer email address
*/
@property(nonatomic, strong) NSString* email;
/**
  	transactiontype enum that represent : Sale and Authorization.
*/
@property(nonatomic) enum RDPConnectAPITransactionType transaction_type;
/**
	Merchant return URL, where customers will be redirected to after payment is completed.
*/
@property(nonatomic, strong) NSString* return_url;
/**
	A maximum of 200 characters. e.g. Blue Jeans XL
*/
@property(nonatomic, strong) NSString* merchant_reference;
/**
	Merchant notification URL.
*/
@property(nonatomic, strong) NSString* notify_url;
/**
	Promotional code
*/
@property(nonatomic, strong) NSString* promo_code;
/**
	First name of cardholder.
*/
@property(nonatomic, strong) NSString* first_name;
/**
	Last name of cardholder.
*/
@property(nonatomic, strong) NSString* last_name;
/**
	Address of cardholder. 
*/
@property(nonatomic, strong) NSString* address_line1;
/**
	Address of cardholder
*/
@property(nonatomic, strong) NSString* address_line2;
/**
	City of cardholder
*/
@property(nonatomic, strong) NSString* address_city;
/**
	Postal code of cardholder
*/
@property(nonatomic, strong) NSString* address_postal_code;
/**
	State of cardholder
*/
@property(nonatomic, strong) NSString* address_state;
/**
	Country of cardholder
*/
@property(nonatomic, strong) NSString* address_country;
/**
	This will be echo back in the response to merchant
*/
@property(nonatomic, strong) NSString* merchant_data1;
/**
	This will be echo back in the response to merchant
*/
@property(nonatomic, strong) NSString* merchant_data2;
/**
	This will be echo back in the response to merchant
*/
@property(nonatomic, strong) NSString* merchant_data3;
/**
	This will be echo back in the response to merchant
*/
@property(nonatomic, strong) NSString* merchant_data4;
/**
	This will be echo back in the response to merchant
*/
@property(nonatomic, strong) NSString* merchant_data5;
/**
	Card number to be passed thru to the bank.
*/
@property(nonatomic, strong) NSString* card_number;
/**
	Expiry date (in MMYY format) to be passed thru to the bank.
*/
@property(nonatomic, strong) NSString* expiry_date;
/**
	CVV2 to be passed thru to the bank. 
*/
@property(nonatomic, strong) NSString* cvv2;
/**
	Contains token information generated by Red Dot Payment to represent card number and its expiry date
*/
@property(nonatomic, strong) NSString* token_id;
/**
	Used to set timeout limit (in seconds) in which transaction has to be completed. 
*/
@property(nonatomic, strong) NSString* order_timeout;
/**
	the delegate variabel for RDPConnectAPIDelegate
*/
@property(nonatomic, strong) id<RDPConnectAPIDelegate> delegate;
/**
	the delegate variabel for ConnectAPIInternalDelegate
*/
@property(nonatomic, strong) id<ConnectAPIInternalDelegate> internalDelegate;

/**
 	Method that called to initialize the delegate of this class 
 	@param delegate the param to set the context for implement ConnectAPIDelegate
 	@returns id the intial of RDPConnectAPI
 */
- (id) initWithDelegate:(id<RDPConnectAPIDelegate>) delegate;
/**
 	Method that called to start request the RDPConnectAPI
 */
- (void) startRequest;
/**
    method that called to check the amount validation that related to its currency especially THB
*/
- (void) checkAmountValidation;

//TODO buang method" dibawah ini
- (void) onErrorWith:(NSString *)error;
- (void) signatureWillValidate;
- (void) signatureDidValidateWithParam: (NSDictionary*) params;
- (void) consentScreenDidDissapear;
- (void) onRequestRejectedWithParams:(NSDictionary*) params andErrorCode:(int) errorCode;
- (void) onRequestFinishedWithParams:(NSDictionary*) params;
- (BOOL) shouldDismissConsentScreen;


@end
