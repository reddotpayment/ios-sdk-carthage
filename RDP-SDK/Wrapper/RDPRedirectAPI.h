//
//  RDPRedirectAPI.h
//  RDPSDK
//
//  Created by Ferico Samuel on 5/11/17.
//  Copyright © 2017 d'Amigos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RDPRedirectAPIPaymentType.h"
#import "RAPIFirstRequest.h"
#import "RedirectAPIInternalDelegate.h"
#import "SignatureUtil.h"
#import "HttpPostRequest.h"
#import "Util.h"
#import "RDPRedirectAPIResponse.h"
#import "RDPRedirectAPIRedirectionResponse.h"
#define RDP_RAPI_API_MODE @"redirection_hosted" 
#define RDP_RAPI_DEV_URL @"https://secure-dev.reddotpayment.com/service/payment-api"
#define RDP_RAPI_PRODUCTION_URL @"https://secure.reddotpayment.com/service/payment-api"

/**
 Set of methods to be implemented to act as a Redirect API (RAPI)
 */
@protocol RDPRedirectAPIDelegate <NSObject>
/**
 Method that called when payment url has been received
 @param url url that use to pay the payment
 @param response response that we get first after request in RDPRAPIResponse Class
 */
- (void) paymentURLDidReceivedWithURL:(NSString*) url andResponse:(RDPRedirectAPIResponse*) response;
/**
 Method that called when request transaction is failed
 @param errorCode HTTP Response Code
 @param description Description Of Error
 */
- (void) onRequestFailedWithErrorCode:(int) errorCode andDescription:(NSString*) description;
/**
 Method that called when request transaction is Finished or Success
 @param response the response that return from server
 */
- (void) onRequestFinishedWithResponseObject:(RDPRedirectAPIRedirectionResponse*) response;

@optional
/**
 Method that called just before the payment request
 */
- (void) paymentInterfaceWillRequest;
/**
 Method that called just after the payment Request
 */
- (void) paymentInterfaceDidRequest;
/**
 Method that called just before the payment url load
 */
- (void) paymentUrlWillLoad;
/**
 Method that called just after the payment url load
 */
- (void) paymentUrlDidLoad;
/**
 Method that called just before the redirection result will request
 */
- (void) redirectionResultWillRequest;
/**
 Method that called just after the redirection result request
 */
- (void) redirectionResultDidRequest;
/**
 Method that called just before the redirection url load
 */
- (void) redirectUrlWillLoad;
/**
 Method that called just after the redirection url load
 */
- (void) redirectUrlDidLoad;
/**
 Method that called just after btnBack has been tap
 */
- (void) btnBackDidTap;
/**
 Method that call when consent screen is will be appear or not.
 This method default falue is NO
 @returns returns YES if you want to dismiss consent screen, otherwise returns NO.
 */
- (BOOL) shouldDismissConsentScreen;
/**
 Method that called just after the consent screen dissapear
 */
- (void) consentScreenDidDissapear;
@end
/**
 	Class to represent a RDP Redirect API (RAPI)
 */
@interface RDPRedirectAPI : NSObject<WsDelegate>

/**
	The merchant id given by RDP when setting up an account.
*/
@property(nonatomic, strong) NSString* mid;
/**
	transactiontype enum that represent : 
	S : Sale Transaction
	A : (Pre) Authorisation
	I : Installment
*/
@property(nonatomic) enum RDPRedirectAPIPaymentType payment_type;
/**
	Merchant defined order-id for the transaction. Used for identifying the transaction request
*/
@property(nonatomic, strong) NSString* order_id;
/**
	In 3 digits ISO-4217 Alphabetical Currency Code format.
*/
@property(nonatomic, strong) NSString* ccy;
/**
		Transaction amount, the numbers only
*/
@property(nonatomic) float amount;
/**
	Merchant's site URL where customer is to be redirected when they chose to press "back" button on RDP's payment page
*/
@property(nonatomic, strong) NSString* back_url;
/**
	Merchant's site URL where RDP is going to redirect Customer once a final result has been received from Bank/Acquirer
*/
@property(nonatomic, strong) NSString* redirect_url;
/**
	Merchant's site URL where a notification will received once a final result of the payment transaction is acquired.
*/
@property(nonatomic, strong) NSString* notify_url;
/**
	the email of customer.
*/
@property(nonatomic, strong) NSString* payer_email;
/**
	Any kind of extra information for merchant to relate with this transaction.
*/
@property(nonatomic, strong) NSString* merchant_reference;
/**
	languages that support
*/
@property(nonatomic, strong) NSString* locale;
/**
	Method that want to user avalaible 1 and 0	
*/
@property(nonatomic, strong) NSString* multiple_method_page;
/**
	the forename to whom the transaction is being billed.
*/
@property(nonatomic, strong) NSString* bill_to_forename;
/**
	the surname to whom the transaction is being billed.
*/
@property(nonatomic, strong) NSString* bill_to_surname;
/**
	the city where the transaction is being billed.
*/
@property(nonatomic, strong) NSString* bill_to_address_city;
/**
	the first line of street address where the transaction is being billed.
*/
@property(nonatomic, strong) NSString* bill_to_address_line1;
/**
	the second line of street address where the transaction is being billed.
*/
@property(nonatomic, strong) NSString* bill_to_address_line2;
/**	
	the Country where the transaction is being billed.
*/
@property(nonatomic, strong) NSString* bill_to_address_country;
/**
	the state where the transaction is being billed.
*/
@property(nonatomic, strong) NSString* bill_to_address_state;
/**
	the Postal Code where the transaction is being billed
*/
@property(nonatomic, strong) NSString* bill_to_address_postal_code;
/**
	the cardholder phone whose the transaction is being billed
*/
@property(nonatomic, strong) NSString* bill_to_phone;
/**
	the forename to whom the item sold is being shipped.
*/
@property(nonatomic, strong) NSString* ship_to_forename;
/**
	the surname to whom the item sold is being shipped.
*/
@property(nonatomic, strong) NSString* ship_to_surname;
/**
	the address city to where the item sold is being shipped
*/
@property(nonatomic, strong) NSString* ship_to_address_city;
/**
	the first line of street address to where the item sold is being shipped.
*/
@property(nonatomic, strong) NSString* ship_to_address_line1;
/**
	the second line of street address to where the item sold is being shipped.
*/
@property(nonatomic, strong) NSString* ship_to_address_line2;
/**
	the country to where the item sold is being shipped.
*/
@property(nonatomic, strong) NSString* ship_to_address_country;
/**
	the state to where the item sold is being shipped. (US and Canada only)
*/
@property(nonatomic, strong) NSString* ship_to_address_state;
/**
	the postal code to where the item sold is being shipped.
*/
@property(nonatomic, strong) NSString* ship_to_address_postal_code;
/**
	the phone number to whom the item sold is being shipped.
*/
@property(nonatomic, strong) NSString* ship_to_phone;
/**
	the key that given by RDP
*/
@property(nonatomic, strong) NSString* secret_key;
/**
	This field indicates the tenure of the installment payment.
*/
@property(nonatomic) int tenor_month;
@property(strong, nonatomic) NSString* payment_url;
@property(strong, nonatomic) NSString* payer_name;
@property(strong, nonatomic) NSString* card_no;
@property(strong, nonatomic) NSString* exp_date;
@property(strong, nonatomic) NSString* cvv2;
@property(strong, nonatomic) NSString* api_mode;

/**
	the delegate variabel for RDPRedirectAPIDelegate
*/
@property(nonatomic, strong) id<RDPRedirectAPIDelegate> delegate;
/**
	the delegate variabel for RedirectAPIInternalDelegate
*/
@property(nonatomic, strong) id<RedirectAPIInternalDelegate> internalDelegate;

/**
 	Method that called to initialize the delegate of this class 
 	@param delegate the param to set the context for implement RedirectAPIDelegate
 	@returns id the intial of RDPRedirectAPIDelegate
 */
- (id) initWithDelegate:(id<RDPRedirectAPIDelegate>) delegate;
/**
 	Method that called to start request the RDPRedirectAPI
 */
- (void) startRequest;
/**
    method that called to check the amount validation that related to its currency especially THB
*/
- (void) checkAmountValidation;


@end
